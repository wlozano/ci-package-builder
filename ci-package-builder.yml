# vi:et:sts=2

variables:
  OSNAME: apertis
  PKG_VERSION_SUFFIX: +apertis
  # we cannot just stick the regex in a variable due to
  # https://gitlab.com/gitlab-org/gitlab/-/issues/35438
  OSNAME_BRANCH_RULE: &osname_branch_rule $CI_COMMIT_BRANCH =~ /^apertis\//
  # run on debian/bullseye and debian/bullseye-security as well,
  # apertis-pkg-pull-updates will always import updates for both
  MIRROR_BRANCH_RULE: &mirror_branch_rule $CI_COMMIT_BRANCH =~ /^debian\/[a-z-]+$/
  OBS_REPOSITORIES: default

  NAME: ${GITLAB_USER_NAME}
  FULLNAME: ${GITLAB_USER_NAME}
  EMAIL: ${GITLAB_USER_EMAIL}
  GIT_AUTHOR_NAME: ${GITLAB_USER_NAME}
  GIT_COMMITTER_NAME: ${GITLAB_USER_NAME}
  CI_AUTH_PROJECT_URL: https://${GITLAB_CI_USER}:${GITLAB_CI_PASSWORD}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}.git/
  GBP_CONF_FILES: "/dev/null"
  LICENSE_BLACKLIST_target: "GPL-3 GPL-3+ AGPL-3 AGPL-3+ AGPL-1 AGPL-1+ LGPL-3 LGPL-3+ BSD-4-Clause MPL-1.1"
  # Set OBS_PREFIX in the project package variables to force uploads to a
  # branch. For instance, by setting it to home:yourusername:branches uploads
  # for apertis:v2020:target will end up in home:yourusername:branches:apertis:v2020:target
  OBS_PREFIX: ""
  DOWNSTREAM_RELEASES_debian_bookworm: $CI_DEFAULT_BRANCH
  DOWNSTREAM_RELEASES_debian_bullseye: apertis/v2022:apertis/v2023
  DOWNSTREAM_RELEASES_debian_buster: apertis/v2021
  STABLE_RELEASES: apertis/v2019:apertis/v2020:apertis/v2021:apertis/v2022
  # Colon-separated list of branches for which upstream pulls should be automerged, e.g. apertis/v2022dev1:apertis/v2022dev2, if the MR consists only of a trivial change
  PKG_UPDATES_AUTOMERGE: $CI_DEFAULT_BRANCH
  DEBIAN_MIRROR: https://deb.debian.org/debian
  # disable the submission to FOSSology for now to speed up the rebase
  # we still have the old-style scanner as the default to actually catch licensing issues
  #FOSSOLOGY_URL: https://fossology.apertis.org
  # enable for downstreams packages, pulling from Apertis (see the pull-mirror job)
  #MIRROR_APT_REPOSITORY_PREFIX: https://repositories.apertis.org/apertis/dists
  #MIRROR_GIT_REPOSITORY_PREFIX: https://gitlab.apertis.org/pkg

  # Maximum size in bytes of build log displayed in the "live" job view
  MAX_BUILDLOG_SIZE: 3000000
  # Size of logs in bytes to display from the end of build log in case it has been truncated
  END_BUILDLOG_SIZE: 10000

  PRISTINE_SOURCE: pristine-lfs-source

  # for testing, set those to force jobs to be run or to be skipped
  # if the job name is contained in any way in the string, the matching action is taken
  #FORCE_JOB_RUN: ""
  #FORCE_JOB_SKIP: ""

  # to disable the auto merge feature set PKG_UPDATES_AUTOMERGE_DISABLED to some value
  #PKG_UPDATES_AUTOMERGE_DISABLED: "1"

  OBS_RUNNER_TAG: obs-runner

  OBS_REBUILD_DISABLED:
    description: |
      Set to "1" to not trigger a rebuild on OBS
    value: ""

  LINTIAN_DISABLED:
    description: |
      Set to "yes" to disable the lintian job. During a rebase a lot
      of lintian failures are expected due a newer lintian version.
    value: "no"

  EXTRA_REPO_BASE_URL:
      description: |
        The APT repository base url. Users can also specify password protected urls using the standard convention
        For instance, "https://username:password@repositories-apertispro.boschdevcloud.com/apertispro/"
      value: ""

  EXTRA_REPO_VERSION:
      description: |
        The APT repository release name to be added.
        For instance, if targetting Apertis release v2021, specify "v2021" as the release name.
        Leave it empty for the release name to be auto-determined from the repository branch name.
      value: ""

  EXTRA_REPO_COMPONENTS:
      description: |
        The APT repository components. Users can specify a list of APT repository components separated by space.
        For instance, "target development sdk hmi"
      value: ""

  PACKAGE_UPDATES_PROJECT_PATH: ${CI_PROJECT_PATH}

  PACKAGE_UPDATES_MR_IID: ${CI_MERGE_REQUEST_IID}

  PACKAGE_UPDATES_TESTS: ""

  BOOTSTRAP_TEST: ""

workflow:
  rules:
    - if: $CI_COMMIT_TAG
      when: never
    - when: always

image: $DOCKER_IMAGE

default:
  interruptible: true
  before_script:
    - mkdir -p .git/info
    - echo '* -text -eol -crlf -ident -filter -working-tree-encoding -export-subst' > .git/info/attributes
    - echo 'debian/changelog merge=dpkg-mergechangelogs' >> .git/info/attributes
    - |
      if [ -f .gitattributes ]
      then
        sed -n "/ filter=lfs/p" .gitattributes >> .git/info/attributes
      fi
    - command -v git && git reset --hard
    - EXTRA_REPO_VERSION=${EXTRA_REPO_VERSION:-${CI_DEFAULT_BRANCH##$OSNAME/}}
    - |
      if [ -n "${EXTRA_REPO_BASE_URL}" -a -n "${EXTRA_REPO_COMPONENTS}" ]
      then
        echo "deb ${EXTRA_REPO_BASE_URL} ${EXTRA_REPO_VERSION} ${EXTRA_REPO_COMPONENTS}" >> /etc/apt/sources.list.d/extra.list
        echo "deb-src ${EXTRA_REPO_BASE_URL} ${EXTRA_REPO_VERSION} ${EXTRA_REPO_COMPONENTS}" >> /etc/apt/sources.list.d/extra.list
      elif [ -n "${EXTRA_REPO_BASE_URL}" -o -n "${EXTRA_REPO_COMPONENTS}" ]
      then
        echo "WARN: Incorrect configuration: both env variables EXTRA_REPO_BASE_URL/EXTRA_REPO_COMPONENTS must be set to enable the extra repositories setting"
      fi

.rules:
  - <<: &force_job_rule_skip
      if: $CI_JOB_NAME =~ $FORCE_JOB_SKIP
      when: never
  - <<: &force_job_rule_run
      if: $CI_JOB_NAME =~ $FORCE_JOB_RUN
      when: on_success
  # NOTE: this is copied into the generated pipeline template below, make sure
  # any edits to one or the other stay in sync!
  - <<: &run_on_mr_xor_branch_rule
      if: $CI_COMMIT_BRANCH && $CI_COMMIT_BRANCH !~ /^apertis\// && $CI_OPEN_MERGE_REQUESTS
      # do not run on a non-release branch if there's a MR open, run only in MR
      # context so we can derive the release from the target branch
      when: never
  - <<: &no_run_on_debian_branches
      if: $CI_COMMIT_BRANCH =~ /^debian\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^debian\//
      when: never
  - &no_run_on_upstream_branches
    - if: $CI_COMMIT_BRANCH =~ /^upstream\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^upstream\//
      when: never
  - &no_run_on_pristine_branches
    - if: $CI_COMMIT_BRANCH =~ /^pristine-/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^pristine-/
      when: never
  # For now hardcode the releases that require folding
  # unfortunately we need this kind of approach to avoid merge backports
  - &no_run_on_folding
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^apertis\/v2021/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^apertis\/v2021/
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^apertis\/v2022/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^apertis\/v2022/
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^apertis\/v2023/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^apertis\/v2023/
      when: never
    - if: $CI_PIPELINE_SOURCE == 'merge_request_event' && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /^apertis\/v2024/ && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^apertis\/v2024/
      when: never

stages:
  - build-env
  - local-pre-source-build
  - update
  - merge
  - license scan
  - local-post-source-build
  - build
  - upload
  - OBS
  - abi-checker
  - lintian
  - lintian-test
  - package-test
  - bootstrap-test
  - cleanup

prepare-build-env:
  stage: build-env
  tags:
    - lightweight
  image: debian:bullseye-slim
  variables:
    GIT_DEPTH: 1
  script:
    - apt-get update
    - apt-get install -y --no-install-recommends dpkg-dev
    - APERTIS_RELEASE_DEFAULT=$(echo $CI_COMMIT_BRANCH | grep -o 'v[0-9]\{4\}\(dev[0-9]\|pre\)\?' || true)
    - test -n "$CI_MERGE_REQUEST_TARGET_BRANCH_NAME" && APERTIS_RELEASE_DEFAULT=$(echo $CI_MERGE_REQUEST_TARGET_BRANCH_NAME | grep -o 'v[0-9]\{4\}\(dev[0-9]\|pre\)\?' || true)
    - APERTIS_RELEASE_DEFAULT=${APERTIS_RELEASE_DEFAULT:-${CI_DEFAULT_BRANCH##$OSNAME/}}
    - APERTIS_RELEASE=${APERTIS_RELEASE:-$APERTIS_RELEASE_DEFAULT}
    - DOCKER_IMAGE_DEFAULT=${CI_REGISTRY}/infrastructure/$OSNAME-docker-images/${APERTIS_RELEASE}-package-source-builder
    - DOCKER_IMAGE=${DOCKER_IMAGE:-$DOCKER_IMAGE_DEFAULT}
    - echo "APERTIS_RELEASE=$APERTIS_RELEASE" | tee -a build.env
    - echo "DOCKER_IMAGE=$DOCKER_IMAGE" | tee -a build.env
    # If OBS_PREFIX is set, make sure there's *exactly* one colon at the end.
    - '[ -n "$OBS_PREFIX" ] && OBS_PREFIX="${OBS_PREFIX%%:}:"'
    # If the debian folder does not exist, the next step are not relevant so stop here.
    - '[ ! -d "debian" ] && exit 0'
    - PACKAGE=$(dpkg-parsechangelog -SSource)
    - echo "PACKAGE=$PACKAGE" | tee -a build.env
    - |
        if [[ $CI_MERGE_REQUEST_TARGET_BRANCH_NAME == $OSNAME/* ]]; then
          SECTION=$CI_MERGE_REQUEST_TARGET_BRANCH_NAME
        elif [[ $CI_COMMIT_BRANCH == $OSNAME/* ]]; then
          SECTION=$CI_COMMIT_BRANCH
        else
          SECTION=$CI_DEFAULT_BRANCH
        fi
    - SECTION=$(echo $SECTION | sed -e 's|[-/]|:|g') # get 'apertis:v2020:security' from apertis/v2020-security
    - COMPONENT_DEFAULT=$(cat debian/apertis/component || test "${CI_COMMIT_BRANCH%%/*}" = debian)
    - COMPONENT=${COMPONENT:-$COMPONENT_DEFAULT}
    - echo "COMPONENT=$COMPONENT" | tee -a build.env
    - SANITIZED_COMPONENT=$(echo -n "$COMPONENT" | tr -c '[:alnum:]' '_') # replace non alphanum with '_', eg. non-free → non_free
    - PROJECT="$OBS_PREFIX$SECTION:$COMPONENT"
    - echo "PROJECT=$PROJECT" | tee -a build.env
    - |
      if [[ $CI_COMMIT_BRANCH != $OSNAME/* ]]; then
        BRANCH_PREFIX="${OBS_PREFIX:-home:$OBS_USER:branches:}"
        BRANCHED_PROJECT="$BRANCH_PREFIX$SECTION:$COMPONENT:$CI_COMMIT_REF_SLUG"
        echo "BRANCHED_PROJECT=$BRANCHED_PROJECT" | tee -a build.env
      fi
    - LICENSE_BLACKLIST_VAR=LICENSE_BLACKLIST_$SANITIZED_COMPONENT # look for variables like "$LICENSE_BLACKLIST_target"
    - LICENSE_BLACKLIST=${LICENSE_BLACKLIST:-${!LICENSE_BLACKLIST_VAR:-}}
    - echo "LICENSE_BLACKLIST=$LICENSE_BLACKLIST" | tee -a build.env
    - RELEASE_BUILD=no
    - |
      : check whether this is a release build
      case "${CI_COMMIT_BRANCH}" in
        $OSNAME/*)
          echo "Current branch ${CI_COMMIT_BRANCH} is a ${OSNAME}/* mainline branch"
          DISTRIBUTION="$(dpkg-parsechangelog -SDistribution)"
          if [ "$DISTRIBUTION" = UNRELEASED ]
          then
            echo "⚠️ debian/changelog says UNRELEASED, not a release build"
          elif [ "$DISTRIBUTION" = "$OSNAME" ]
          then
            echo "🎁 Release build for $OSNAME"
            RELEASE_BUILD=yes
          else
            echo "⚠️ debian/changelog targets the distribution '$DISTRIBUTION', add an entry targeting '$OSNAME' to release"
            exit 1
          fi
          ;;
      esac
    - echo "RELEASE_BUILD=$RELEASE_BUILD" | tee -a build.env
    - echo "PACKAGE_UPDATES_PROJECT_PATH=${PACKAGE_UPDATES_PROJECT_PATH}" | tee -a build.env
    - echo "PACKAGE_UPDATES_MR_IID=${PACKAGE_UPDATES_MR_IID}" | tee -a build.env
    - PACKAGE_UPDATES_TESTS=$(cat debian/apertis/automated-tests 2> /dev/null || true)
    - echo "PACKAGE_UPDATES_TESTS=${PACKAGE_UPDATES_TESTS}" | tee -a build.env
    - if [ -f debian/apertis/bootstrap-test ] ; then BOOTSTRAP_TEST=1; fi
    - echo "BOOTSTRAP_TEST=${BOOTSTRAP_TEST}" | tee -a build.env
  artifacts:
    reports:
      dotenv: build.env
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - *no_run_on_pristine_branches
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
    - when: never

local-pipeline-pre-source-build-compat:
  # debian/apertis/local-gitlab-ci.yml is deprecated in favor of
  # debian/apertis/local-pre-source-build-gitlab-ci.yml to match
  # debian/apertis/local-post-source-build-gitlab-ci.yml
  stage: local-pre-source-build
  trigger:
    strategy: depend
    include:
      - local: debian/apertis/local-gitlab-ci.yml
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - exists:
      - debian/apertis/local-gitlab-ci.yml

local-pipeline-pre-source-build:
  stage: local-pre-source-build
  trigger:
    strategy: depend
    include:
      - local: debian/apertis/local-pre-source-build-gitlab-ci.yml
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - exists:
      - debian/apertis/local-pre-source-build-gitlab-ci.yml

pull-mirror:
  # this job is not used on the Apertis infrastructure but it provides a
  # reference for downstreams that want to mirror Apertis repositories and fetch
  # updates from them
  stage: update
  tags:
    - lightweight
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - if: $MIRROR_APT_REPOSITORY_PREFIX == null || $MIRROR_APT_REPOSITORY_PREFIX == ""
      when: never
    - if: $MIRROR_GIT_REPOSITORY_PREFIX == null || $MIRROR_GIT_REPOSITORY_PREFIX == ""
      when: never
    - if: *mirror_branch_rule
      when: on_success
  script:
    - git checkout "${CI_COMMIT_BRANCH}" # to use git merge we cannot work on the detached HEAD GitLab sets up
    - PACKAGE=$(dpkg-parsechangelog -SSource || echo "$CI_PROJECT_NAME")
    - wget "$MIRROR_APT_REPOSITORY_PREFIX/${APERTIS_RELEASE}/${COMPONENT}/source/Sources.gz"
    - gunzip Sources.gz
    - PULL_VERSION=${PULL_VERSION:-$(grep-dctrl --exact-match -FPackage -sVersion -n "$PACKAGE" Sources)}
    - PULL_TAG=${PULL_TAG:-$(echo $PULL_VERSION | perl -pe 'y/:~/%_/; s/\.(?=\.|$|lock$)/.#/g;')}
    - git remote add upstream "$MIRROR_GIT_REPOSITORY_PREFIX/${PACKAGE}.git"
    - git fetch upstream --tags
    - git merge --ff-only "apertis/$PULL_TAG"
    - |
        if git branch pristine-lfs upstream/pristine-lfs
        then
          git lfs fetch --all upstream upstream/pristine-lfs
          echo "Push pristine-lfs"
          # Avoid pack-objects running out of memory on bigger repositories
          git config pack.windowMemory 512m
          # Work around git-lfs in buster erroring out if lfs files are missing
          # even if they're on the server.
          git config lfs.allowincompletepush true
          git lfs push --all ${CI_AUTH_PROJECT_URL} pristine-lfs
          git push ${CI_AUTH_PROJECT_URL} pristine-lfs
        else
          echo "No pristine-lfs; skipping"
        fi
    # skip CI since we're pushing to our own branch the latest update, so we'd
    # end up re-running the same pipeline as a no-op
    - git push -o ci.skip --follow-tags "${CI_AUTH_PROJECT_URL}"

pull-updates:
  stage: update
  tags:
    - lightweight
  resource_group: $CI_PROJECT_ID
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - if: $DEBIAN_MIRROR == null || $DEBIAN_MIRROR == ""
      when: never
    - if: *mirror_branch_rule
      when: on_success
  script:
    - |
      if [ "$CI_COMMIT_BRANCH" = "debian/unstable" ]
      then
        echo "🛑 Used upstream branch is 'debian/unstable', please instead use 'debian/sid' for consistency across all packages!"
        exit 1
      fi
    - PACKAGE=$(dpkg-parsechangelog -SSource || echo "$CI_PROJECT_NAME")
    # normalize the release information from the current branch
    - PULL_RELEASE_BRANCH=${CI_COMMIT_BRANCH%%-*} # debian/buster-security → debian/buster
    - PULL_RELEASE_VENDOR=${PULL_RELEASE_BRANCH%%/*} # debian/buster → debian
    - PULL_RELEASE_NAME=${PULL_RELEASE_BRANCH##*/} # debian/buster → buster
    - echo "Pull $PACKAGE updates from $PULL_RELEASE_VENDOR $PULL_RELEASE_NAME"
    - set -x;
      apertis-pkg-pull-updates
        --package=${PACKAGE}
        --upstream=${PULL_RELEASE_NAME}
        --mirror=${DEBIAN_MIRROR};
      set +x
    # Avoid pack-objects running out of memory on bigger repositories
    - git config pack.windowMemory 512m
    # Work around git-lfs in buster erroring out if lfs files are missing
    # even if they're on the server.
    - git config lfs.allowincompletepush true
    - BRANCHES=$(git branch -l --format='%(refname:lstrip=2)'
        "${PULL_RELEASE_VENDOR}/*"
        "upstream/*"
        "pristine-lfs"
      )
    - echo "🔼 Pushing the updated local branches" $BRANCHES
    - git push --follow-tags --atomic ${CI_AUTH_PROJECT_URL} $BRANCHES

merge-updates:
  stage: merge
  resource_group: $CI_PROJECT_ID
  tags:
    - lightweight
  variables:
    # needed to ensure the current branch has been refreshed, as the previous job may have update it
    # by importing updates and we do not want to try to merge an outdated commit
    # (see also how we pass the ci.skip options to not re-kick this pipeline when importing updates)
    GIT_STRATEGY: clone
    GIT_DEPTH: 0
    REBASE_RANGE: '' # overridden externally when mirroring new downstream branches, e.g. apertis/v2021..downstream/v2021
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - if: *mirror_branch_rule
      when: on_success
  script:
    - UPSTREAM_RELEASE=$(echo $CI_COMMIT_BRANCH | sed -E 's/-(security|updates|backports|proposed-updates)$//')
    - VAR_SUFFIX=$(echo $UPSTREAM_RELEASE | sed 's/\//_/g')
    - DOWNSTREAM_RELEASES_VAR=DOWNSTREAM_RELEASES_${VAR_SUFFIX} # look for variables like "$DOWNSTREAM_RELEASES_debian_bullseye"
    - AUTO_MERGE_ARG=""
    - |
      if [ -z "$DOWNSTREAM_RELEASES" ]
      then
        echo "Checking downstream targets in ${DOWNSTREAM_RELEASES_VAR}"
        DOWNSTREAM_RELEASES=${!DOWNSTREAM_RELEASES_VAR} # expand variables like DOWNSTREAM_RELEASES_debian_bullseye
      fi
    - |
      if [ -z "$DOWNSTREAM_RELEASES" ]
      then
        echo "Falling back to $CI_DEFAULT_BRANCH as the downstream target"
        DOWNSTREAM_RELEASES=$CI_DEFAULT_BRANCH
      fi
    - |
      if [ -z "${PKG_UPDATES_AUTOMERGE_DISABLED}" ] && [ -n "${PKG_UPDATES_AUTOMERGE}" ]
      then
        echo "Enabling auto merge of trivial changes for ${PKG_UPDATES_AUTOMERGE}"
        AUTO_MERGE_ARG="--auto-merge=${PKG_UPDATES_AUTOMERGE}"
      fi
    - |
      if [ -n "${REBASE_RANGE}" ]
      then
        echo "Enabling downstream branch creation for ${DOWNSTREAM_RELEASES} and rebasing changes from ${REBASE_RANGE}"
        REBASE_RANGE_ARG="--rebase-range=${REBASE_RANGE}"
      fi
    - >
      apertis-pkg-merge-upstream-to-downstreams
      --osname=${OSNAME}
      --upstream=${UPSTREAM_RELEASE}
      --downstreams=${DOWNSTREAM_RELEASES}
      --stable=${STABLE_RELEASES}
      --local-version-suffix=${PKG_VERSION_SUFFIX}
      ${REBASE_RANGE_ARG}
      ${AUTO_MERGE_ARG}
      ${CI_AUTH_PROJECT_URL}

scan-licenses:
  stage: license scan
  tags:
    - lightweight
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: *mirror_branch_rule
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^upstream\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^upstream\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^debian\// || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^debian\//
      when: never
    - if: $CI_COMMIT_BRANCH =~ /^pristine-/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /^pristine-/
      when: never
    # the licensing scan framework is really only supported on the v2022 channel onward
    - if: $CI_COMMIT_BRANCH =~ /v2020/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /v2020/
      when: never
    - if: $CI_COMMIT_BRANCH =~ /v2021/ || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /v2021/
      when: never
    - when: on_success

  dependencies:
    - prepare-build-env
  script:
    - apt-get update
    - apt-get install -y --no-install-recommends curl jq
    # the license blacklist can be set on per-project/per-section basic
    - dpkg-source --before-build .
    - |
      if [ -e debian/apertis/pre-scan-hook ]
      then
        echo "Executing debian/apertis/pre-scan-hook"
        debian/apertis/pre-scan-hook
      fi
    - FAIL_ON_UNKNOWN=--fail-on-unknown
    - |
      if [ -e debian/apertis/copyright-long ]
      then
        echo "Using the long list format (no wildcards)"
        LONG_LIST=--long-list
      fi
    # do not fail on unknown licenses if we do not have a blacklist anyway
    - test -z "${LICENSE_BLACKLIST}" && FAIL_ON_UNKNOWN=""
    - BRANCH=${CI_COMMIT_BRANCH:-$CI_MERGE_REQUEST_SOURCE_BRANCH_NAME}
    - ci-license-scan --fossology-host "${FOSSOLOGY_URL}"
            --fossology-username "${FOSSOLOGY_USERNAME}" --fossology-password "${FOSSOLOGY_PASSWORD}"
            --source-url "https://${CI_SERVER_HOST}/${CI_PROJECT_PATH}" --source-branch "${BRANCH}"
            $FAIL_ON_UNKNOWN
            --blacklist-licenses "${LICENSE_BLACKLIST}"
            $LONG_LIST
    - dpkg-source --after-build .
    - |
      case $BRANCH in
        $OSNAME/*)
          if ! diff debian/apertis/copyright.new debian/apertis/copyright
          then
            echo "Copyright report needs to be updated, please submit a MR"
          else
            echo "Copyright report is up to date"
          fi
          rm debian/apertis/copyright.new
        ;;
        *)
          MERGE_ON_SUCCESS=`curl https://${CI_SERVER_HOST}/api/v4/projects/${CI_PROJECT_PATH}/merge_requests/${CI_MERGE_REQUEST_IID}/ | jq -r '.merge_when_pipeline_succeeds'`
          PUSH_OPTIONS=""
          if [ "$MERGE_ON_SUCCESS" = "true" ] ; then
            PUSH_OPTIONS="-o merge_request.merge_when_pipeline_succeeds -o merge_request.remove_source_branch"
          fi
          mv debian/apertis/copyright.new debian/apertis/copyright
          git add -f debian/apertis/copyright
          if git commit -s -m 'Refresh the automatically detected licensing information' debian/apertis/copyright
          then
            set -x
            git push ${PUSH_OPTIONS} ${CI_AUTH_PROJECT_URL} HEAD:${BRANCH}
            set +x
          fi
        ;;
      esac
  artifacts:
    when: on_failure
    paths:
      - debian/apertis/copyright
      - debian/apertis/copyright.new
      - debian/apertis/copyright.fossology
      - debian/fill.copyright.blanks.yml
      - debian/copyright-scan-patterns.yml

build-source:
  variables:
    GIT_STRATEGY: clone
    GIT_DEPTH: 1
  # avoid conflicts when concurrently trying to commit stuff to the shared $PRISTINE_SOURCE branch
  resource_group: $CI_PROJECT_ID
  interruptible: false
  stage: build
  tags:
    - lightweight
  script:
    - |
      if [ "$RELEASE_BUILD" = yes ]
      then
        echo "🎁 Release build"
      else
        echo "⚙️  Test build"
      fi
    - git ls-remote --exit-code -h origin pristine-lfs && git fetch origin --progress pristine-lfs
    - git ls-remote --exit-code -h origin "$PRISTINE_SOURCE" && git fetch origin --progress "$PRISTINE_SOURCE"
    - VERSION=$(dpkg-parsechangelog -SVersion)
    - VERSIONTAG=$OSNAME/$(echo "${VERSION}" | sed -e 'y/:~/%_/' -e 's/\.\(\.\|$\|lock$\)/.\1#/g')
    - echo "debian/changelog points to ${VERSION}, looking for tag ${VERSIONTAG}"
    - |
      if git describe --tags --match "${VERSIONTAG}" --exact-match > /dev/null 2>&1
      then
        echo "Fetching the already tagged source package version for this commit"
        pristine-lfs -v checkout --full --branch ${PRISTINE_SOURCE} --auto -o _build/
        echo "DSC=$(echo _build/*.dsc)" | tee -a build.env
        exit 0
      fi
    - |
      if git ls-remote --exit-code -t origin "${VERSIONTAG}"
      then
        echo "🛑 Version ${VERSION} already released and tagged as ${VERSIONTAG}"
        echo "Bump the debian/changelog version before proceeding."
        git ls-remote -t origin "$VERSIONTAG"
        exit 1
      fi
    - echo "Building the ${VERSION} source package"
    - gbp buildpackage
        --git-tag-only
        --git-ignore-branch
        --git-ignore-new
        --git-debian-tag="$OSNAME/%(version)s"
        --git-debian-tag-msg="%(pkg)s $OSNAME release %(version)s"
    - test "$RELEASE_BUILD" = yes || POSTEXPORT="deb-git-version-gen --distro=$OSNAME --dch"
    - ci-buildpackage
        --git-postexport="${POSTEXPORT:-}"
        --git-builder='debuild --no-lintian -i -I'
        --git-prebuild='debian/rules debian/control || true'
        -S
    - |
      if dpkg-parsechangelog | grep -q 'PLEASE SUMMARIZE'
      then
        echo "🛑 debian/changelog says PLEASE SUMMARIZE"
        echo "Add a summary of the remaining downstream changes"
        exit 1
      fi
    - DSC=$(echo _build/*.dsc)
    - echo "DSC=$DSC" | tee -a build.env
    - |
      if [ "$RELEASE_BUILD" != yes ]
      then
        echo "✨ Non-release build completed, ${DSC}"
        exit 0
      fi
    - echo "✨ Release build completed, uploading ${DSC} to ${PRISTINE_SOURCE} and tagging ${VERSIONTAG}"
    # Work around git-lfs in buster erroring out if lfs files are missing
    # even if they're on the server.
    - git config lfs.allowincompletepush true
    - pristine-lfs -v import-dsc --full --branch "${PRISTINE_SOURCE}" "${DSC}"
    - git push --atomic "${CI_AUTH_PROJECT_URL}" "${PRISTINE_SOURCE}" "${VERSIONTAG}"
    - echo "✨ Release pushed and tagged as ${VERSIONTAG}"
  artifacts:
    paths:
      - _build/
    when: always
    reports:
      dotenv: build.env
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - if: *mirror_branch_rule
      when: never
    - exists:
      - debian/changelog
      - debian/apertis/component
      when: on_success
    - when: never
  dependencies:
    - prepare-build-env

local-pipeline-post-source-build:
  stage: local-post-source-build
  trigger:
    strategy: depend
    include:
      - local: debian/apertis/local-post-source-build-gitlab-ci.yml
  variables:
    PARENT_PIPELINE_ID: $CI_PIPELINE_ID
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    - *run_on_mr_xor_branch_rule
    - exists:
      - debian/apertis/local-post-source-build-gitlab-ci.yml

upload:
  stage: upload
  interruptible: false
  tags:
    - $OBS_RUNNER_TAG
  resource_group: obs-$CI_COMMIT_REF_NAME
  variables:
    JOB_RULES: |
      # NOTE: this is copied from the .rules template at the top of this
      # file, make sure any edits to one or the other stay in sync!
      # This needs to be repeated here to avoid errors:
      # https://gitlab.com/gitlab-org/gitlab/-/issues/276179#note_700305979
      - if: $$CI_COMMIT_BRANCH && $$CI_OPEN_MERGE_REQUESTS
        # do not run on a branch if there's a MR open, run only in MR context so
        # we can derive the release from the target branch
        when: never
      - when: on_success
    REBUILD_VALUE: 'true'
  # Clear the default value, which uses shell syntax.
  before_script: []
  script:
    - >
      dput $PROJECT $DSC
        --branch-to $BRANCHED_PROJECT
        --rebuild-if-unchanged=$REBUILD_VALUE
    - >
      generate-monitor $OBS_RUNNER_TAG
        --rules $JOB_RULES
        --artifact-expiration '3 days'
        --download-build-results-to 'results/'
  after_script:
    - prune --ignore-missing-build-info --only-if-job-unsuccessful
  artifacts:
    paths:
      - build-info.yml
      - obs.yml
    when: always
  environment:
    name: upload/$CI_COMMIT_REF_NAME
    # Ideally this would point to the obs project,
    # but we can't mangle variables enough
    url: https://build.collabora.co.uk
    on_stop: cleanup
  rules:
    - &upload_rules
      - if: >
          $OBS_USER == null || $OBS_USER == ""
          || $OBS_PASSWORD == null || $OBS_PASSWORD == ""
        when: never
      - *force_job_rule_skip
      - *force_job_rule_run
      - *run_on_mr_xor_branch_rule
      - *no_run_on_debian_branches
      - *no_run_on_upstream_branches
      - *no_run_on_pristine_branches
      - *no_run_on_folding
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
    - if: $OBS_REBUILD_DISABLED == "1"
      variables:
        REBUILD_VALUE: 'false'
    - when: never
  dependencies:
    - prepare-build-env
    - build-source

obs:
  stage: OBS
  needs:
    - upload
  resource_group: obs-$CI_COMMIT_REF_NAME
  trigger:
    strategy: depend
    include:
      - artifact: obs.yml
        job: upload
  rules:
    - *upload_rules
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
    - when: never

abi-checker:
  stage: abi-checker
  dependencies:
    - prepare-build-env
  script:
    # Retrieve again new binary packages from OBS
    # They are already captured as artifacts but in a child pipeline that prevents
    # us to directly use them. See https://gitlab.com/gitlab-org/gitlab/-/issues/285100
    # So, we need to download them from OBS or use the GitLab API to retrieve them
    # from the child pipeline, but this solution seems to have weaknesses.
    - ARCHITECTURE=x86_64
    - OBS_ARCH=$(osc repositories "$PROJECT" "$PACKAGE" | grep "$ARCHITECTURE")
    - |
      if [ -n "$OBS_ARCH" ]
      then
        osc getbinaries -d results "$PROJECT" "$PACKAGE" "$OBS_REPOSITORIES" "$ARCHITECTURE"
        apertis-abi-compare results
      else
        echo "🛑 abi-checker job requires $ARCHITECTURE to be enabled on OBS!"
        exit 1
      fi
  artifacts:
    paths:
      - 'compat_reports/*'
      - '*/log.txt'
    when: always
    expire_in: 1 week
  rules:
    - *upload_rules
    - if: $OSC_CONFIG == null || $OSC_CONFIG == ""
      when: never
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      exists:
        - debian/apertis/abi-compare
      when: on_success

lintian:
  stage: lintian
  dependencies:
    - prepare-build-env
  script:
    # Retrieve again new binary packages from OBS.
    # They are already captured as artifacts but in a child pipeline that prevents
    # us to directly use them. See https://gitlab.com/gitlab-org/gitlab/-/issues/285100
    # So, we need to download them from OBS or use the GitLab API to retrieve them
    # from the child pipeline. Using the GitLab API requires to use project tokens which
    # is not a problem for Apertis, but would require adjustments for Apertis derivatives.
    - LIST_ARCH_PKGS=arch_built.txt
    - LIST_REPO=list_repo.txt
    - LIST_DEB=list_deb.txt
    - |
      if [[ -n "$BRANCHED_PROJECT" ]]; then
        PROJECT="$BRANCHED_PROJECT"
      fi
    - |
      osc repositories "$PROJECT" "$PACKAGE" |
        grep "^$OBS_REPOSITORIES" |
        awk '{print $2}' > "$LIST_REPO"
    - |
      if [ -s "$LIST_REPO" ]; then
        echo "🌟 OBS has repositories configured for this project!"
      else
        echo "🛑 OBS doesn't have any repositories configured for this project!"
        exit 1
      fi
    - |
      while read -r REPO
      do
        echo "🔍 Listing ${REPO} binary packages from OBS:"
        osc ls -b "$PROJECT" "$PACKAGE" "$OBS_REPOSITORIES" "$REPO" > "$LIST_DEB" || true
        if grep -q "\.deb$" "$LIST_DEB"; then
          echo "🌟 OBS produced binary packages for ${REPO}!"
          echo "🔍 Lintian will check them!"
          echo "$REPO" >> "$LIST_ARCH_PKGS"
        else
          echo "🛑 OBS did not produce any binary packages for ${REPO}!"
        fi
      done < "$LIST_REPO"
    - |
      while read -r ARCH
      do
        echo "🔍 Retrieving ${ARCH} binary packages from OBS:"
        osc getbinaries -d "$ARCH" "$PROJECT" "$PACKAGE" "$OBS_REPOSITORIES" "$ARCH"
      done < "$LIST_ARCH_PKGS"
    - LINTIAN_VERSION=$(lintian --print-version)
    - |
      if dpkg --compare-versions "$LINTIAN_VERSION" lt "2.77.0"; then
        FAIL_ON_ARG="--fail-on-warnings"
      else
        FAIL_ON_ARG="--fail-on error,warning"
      fi
    - LINTIAN_ARGS="--verbose --info --color always --show-overrides $FAIL_ON_ARG --profile apertis --allow-root"
    - |
      while read -r ARCH
      do
        echo "🔍 Running lintian on ${ARCH} packages:"
        lintian $LINTIAN_ARGS ${ARCH}/*.deb | tee lintian_results_${ARCH}.txt
        echo -e "\n"
      done < "$LIST_ARCH_PKGS"
  artifacts:
    paths:
      - 'lintian_results_*.txt'
    when: always
    expire_in: 1 week
  rules:
    - *upload_rules
    - if: $OSC_CONFIG == null || $OSC_CONFIG == ""
      when: never
    - if: $LINTIAN_DISABLED == "yes"
      when: never
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      exists:
        - debian/apertis/lintian
      when: on_success

lintian-errors:
  inherit:
    variables: false
  stage: lintian-test
  trigger:
    project: tests/lintian-errors
    strategy: depend
  rules:
    - *upload_rules
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
      exists:
        - debian/apertis/lintian-profile
      when: on_success

lintian-errors-test:
  stage: lintian-test
  dependencies:
    - prepare-build-env
  needs:
    - prepare-build-env
    - lintian-errors
  script:
    # Download and install newly built lintian from OBS
    - |
      if [[ -n "$BRANCHED_PROJECT" ]]; then
        PROJECT="$BRANCHED_PROJECT"
      fi
    - osc getbinaries -d results "$PROJECT" "lintian" "$OBS_REPOSITORIES" "x86_64"
    - dpkg -i results/lintian_*_all.deb
    # Install missing lintian dependencies
    - apt update && apt --fix-broken install -y
    # Retrieve pkg for tests/lintian-errors from OBS
    - PROJ_LINTERR="home:apertis-gitlab-test:branches:test:apertis:${CI_DEFAULT_BRANCH##$OSNAME/}:target"
    - osc getbinaries -d tests "$PROJ_LINTERR" "lintian-errors" "$OBS_REPOSITORIES" "x86_64"
    # Test with newly built lintian
    - LINTIAN_VERSION=$(lintian --print-version)
    - |
      if dpkg --compare-versions "$LINTIAN_VERSION" lt "2.77.0"; then
        FAIL_ON_ARG="--fail-on-warnings"
      else
        FAIL_ON_ARG="--fail-on error,warning"
      fi
    - LINTIAN_ARGS="--verbose --info --color always --show-overrides $FAIL_ON_ARG --profile apertis --allow-root"
    - lintian $LINTIAN_ARGS tests/lintian-errors_*_all.deb | tee lintian-errors_results.txt
  artifacts:
    paths:
      - 'lintian-errors_results.txt'
    when: always
    expire_in: 1 week
  rules:
    - *upload_rules
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
      exists:
        - debian/apertis/lintian-profile
      when: on_success

package-test:
  stage: package-test
  variables:
    PACKAGE_UPDATES_TESTS: $PACKAGE_UPDATES_TESTS
  trigger:
    project: infrastructure/apertis-image-recipes
    branch: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    strategy: depend
  rules:
    - if: $CI_PIPELINE_SOURCE != 'merge_request_event'
      when: never
    - *upload_rules
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
      exists:
        - debian/apertis/automated-tests
      when: on_success

bootstrap-test:
  stage: bootstrap-test
  variables:
    BOOTSTRAP_TEST: $BOOTSTRAP_TEST
  trigger:
    project: infrastructure/apertis-image-recipes
    branch: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
    strategy: depend
  rules:
    - if: $CI_PIPELINE_SOURCE != 'merge_request_event'
      when: never
    - *upload_rules
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: on_success
      exists:
        - debian/apertis/bootstrap-test
      when: on_success

cleanup:
  stage: cleanup
  tags:
    - $OBS_RUNNER_TAG
  needs:
    - prepare-build-env
    - upload
  resource_group: obs-$CI_COMMIT_REF_NAME
  # Clear the defaults.
  before_script: []
  script:
    - prune
  environment:
    name: upload/$CI_COMMIT_REF_NAME
    action: stop
  rules:
    - *upload_rules
    - if: *osname_branch_rule
      when: never
    - if: $CI_COMMIT_BRANCH || $CI_MERGE_REQUEST_TARGET_BRANCH_NAME
      when: manual
      allow_failure: true
    - when: never

freeze-stable-branches:
  stage: build-env
  image: alpine
  tags:
    - lightweight
  variables:
    GIT_STRATEGY: none
  script:
    - echo Do not use ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME} directly as it is a published stable release.
    - echo Use ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-updates or ${CI_MERGE_REQUEST_TARGET_BRANCH_NAME}-security instead.
    - "false"
  rules:
    - *force_job_rule_skip
    - *force_job_rule_run
    # only run on merge requests
    - if: $CI_MERGE_REQUEST_ID == ""
      when: never
    # always allow merges coming from an `apertis/*-{updates,security}` branch, for instance from `apertis/v2020-updates` to `apertis/v2020`
    - if: $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /apertis\// && $CI_MERGE_REQUEST_SOURCE_BRANCH_NAME =~ /(-updates|-security)$/
      when: never
    # block merges to stable branches
    - if: $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /apertis\// && $CI_MERGE_REQUEST_TARGET_BRANCH_NAME =~ /(v2019|v2020|v2021|v2022)$/
      when: always
    # always allow merges to other branches
    - when: never
